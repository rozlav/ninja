# Test charges/décharges 18650 de récup 
## 1. Contexte
Matériel de test : OPUS BT-C3100
ressource : https://www.youtube.com/watch?v=MPiLKDNLM9A

## 2. Premier tri
 - Si + de 2.5 (LABEL « OK ») c'est ok
 - Si entre 1.5 et 2.5 (LABEL « MOYEN »)c'est possible de faire revivre sur les bonnes marques de piles en chargeant à 200mA
 - Si en dessous de 1.5 (LABEL « ELON MUSK EST UNE MERDE ») c'est mort
 
## 3. Procédure de test charge
3.1 recharge en mode « CHARGE-TEST » (ne doit pas chauffer ⚠️, sinon jeter, 500mA maximum)
3.2 Attendre 1 à 2 semaines MAX
3.3 Résultats et deuxième tri 
 - si au dessus 4.1 c'est ok ! Décharge lente cool !
 - si entre 4.0 et 4.1 c'est ok, décharge rapide BOF 
 - si en dessous 4.0 alors c'est pas bon
3.4 Décharger les bonnes (+4.1+) à 700mA, attendre quelques heures, aller à la page mAh, voir la capacité

## 4. Trier les décharges lentes par capacité
## 5. Trier les décharges moyennes par capacité
