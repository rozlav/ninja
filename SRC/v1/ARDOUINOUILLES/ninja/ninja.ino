/*
WTFPL
what the fuck you want license
DISCLAIMER :
*/

#include <FastLED.h>
#define numLeds 95 // how much leds in the strip
CRGB leds[numLeds]; // define leds array
int levelLightDelay[] = { 250, 75, 50 }; // how much time the led blinks before the stick drops, per level 

int magnets[] = {6, 7, 8, 9, 10, 11 }; // magnets pinout
int num_magnets = sizeof(magnets) / sizeof(magnets[0]);

float levels_delay_redution[] = {0.7, 0.4, 0.3, 0}; // speed factor for the drop timing
int levels_button_pin[] = { 2, 3, 4, 5 }; // last level is for tests
int num_levels = sizeof(levels_delay_redution) / sizeof(levels_delay_redution[0]);

int selected_level = -1;

unsigned long currentTime; // used for led animations

// the setup function runs once when you press reset or power the board
void setup() {
  delay( 1000 ); // power-up safety delay

  //set up led strip
  FastLED.addLeds<WS2812, 13, GRB>(leds, numLeds);
  FastLED.setBrightness( 40 );
  
  for (int i = 0; i < num_magnets; i++) {
    pinMode(magnets[i], OUTPUT);
    delay(10);
    digitalWrite(magnets[i], HIGH);
  }

  for (int i = 0; i < num_levels; i++) {
    pinMode(levels_button_pin[i], INPUT_PULLUP);
  }

  Serial.begin(9600);
}


void start_game(int selected_level) {

  if(selected_level != 2){
    fill_solid(leds, numLeds, CRGB( 127, 30, 0));
    FastLED.show();
  }else{
    fill_solid(leds, numLeds, CRGB( 0, 50, 0));
    FastLED.show();
  }

  
	Serial.print("Let's the game begin! The difficulty is" );
	Serial.println(selected_level);
	int delay_min = (int)(1000 * levels_delay_redution[selected_level]);
	int delay_max = (int)(4500 * levels_delay_redution[selected_level]);
	Serial.print("Min delay: ");
	Serial.println(delay_min);
	Serial.print("Max delay: ");
	Serial.println(delay_max);
  
	// Wait for 3 seconds before starting game
	delay(3000);
  
	// Turn the magnet off in random order
	for (int i = 0; i < num_magnets; i++) {
	  
	  // choose a random stick  
	  int j = random(0, num_magnets);
	  while (digitalRead(magnets[j]) == LOW) {
	    j = random(0, num_magnets);
	  }

    if(selected_level != 2){
      // blink leds
      for (int k = j*15 ; k < (j+1)*15 ; k++) {
          leds[k].setRGB( 255,255,255);
      }
      FastLED.show();
      delay(levelLightDelay[selected_level]);
      fill_solid(leds, numLeds, CRGB( 255, 255, 255));
      FastLED.show();
      delay(50);
      fill_solid(leds, numLeds, CRGB( 255, 50, 0));
      FastLED.show();
    }

    // drop it !
	  digitalWrite(magnets[j], LOW);
		Serial.print("Dropping stick n. ");
		Serial.println(j);
    // last stick drop faster !
    if( i == num_magnets-1){
       delay(random(delay_min, 1100));
    }else{
	    delay(random(delay_min, delay_max));
    }
	}

	Serial.println("Game is done, re-activating the magnets");
	for (int i = 0; i < num_magnets; i++) {
	  digitalWrite(magnets[i], HIGH);
	}

}

// the loop function runs over and over again forever
void loop() {
  for (int i = 0; i < num_levels; i++) {
	  bool level_state = digitalRead(levels_button_pin[i]);
	  if ( level_state == LOW ) {
		 	selected_level = i; 
			Serial.print("User selected level ");
			Serial.println(selected_level);

	  }

  // slowly pulsating blueish, idle style
  float currentTime = millis()/float(1000); 
  float color = sin(currentTime)*127+127;
  fill_solid(leds, numLeds, CRGB( 0, color/6+10, color/4+20));
  FastLED.show();
}


  if ( selected_level >= 0 ) {
		Serial.print("Starting game with level ");
		Serial.println(selected_level);
	  start_game(selected_level);
	  selected_level = -1;
  }

}
