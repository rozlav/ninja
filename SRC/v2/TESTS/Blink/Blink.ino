#define LED17 17
#define LED16 16
#define LED4 4
#define LED0 0
#define LED2 2
#define LED15 15

#define MAGNET1 18
#define MAGNET2 19
#define MAGNET3 13
#define MAGNET4 12
#define MAGNET5 27
#define MAGNET6 14



void setup() {
  // initialize digital pin magnets & leds as an output.
  pinMode(LED17,OUTPUT);
  pinMode(LED16,OUTPUT);
  pinMode(LED4,OUTPUT);
  pinMode(LED0,OUTPUT);
  pinMode(LED2,OUTPUT);
  pinMode(LED15,OUTPUT);

  pinMode(MAGNET1, OUTPUT);
  pinMode(MAGNET2, OUTPUT);
  pinMode(MAGNET3, OUTPUT);
  pinMode(MAGNET4, OUTPUT);
  pinMode(MAGNET5, OUTPUT);
  pinMode(MAGNET6, OUTPUT);
}

void loop() {

  
  delay(2000);
  digitalWrite(LED17,HIGH);
  digitalWrite(LED16,HIGH);
  digitalWrite(LED4,HIGH);
  digitalWrite(LED0,HIGH);
  digitalWrite(LED2,HIGH);
  digitalWrite(LED15,HIGH);

  digitalWrite(MAGNET1, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET2, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET3, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET4, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET5, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET6, HIGH);   // turn the magnet on (HIGH is the voltage level)


  delay(2000);
  
  digitalWrite(LED17,LOW);
  digitalWrite(LED16,LOW);
  digitalWrite(LED4,LOW);
  digitalWrite(LED0,LOW);
  digitalWrite(LED2,LOW);
  digitalWrite(LED15,LOW);

  digitalWrite(MAGNET1, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET2, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET3, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET4, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET5, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET6, LOW);    // turn the magnet off by making the voltage LOW


}
