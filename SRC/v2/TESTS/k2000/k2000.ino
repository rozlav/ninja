int bulbs[] = {17, 16, 4, 0, 2, 15}; // Array of pin numbers for each LED
int num_bulbs = sizeof(bulbs) / sizeof(bulbs[0]);

void setup() {
  Serial.begin(115200);
  
  for (int i = 0; i < num_bulbs; i++) {
    pinMode(bulbs[i], OUTPUT); // Set LED pins as OUTPUT
    digitalWrite(bulbs[i], LOW); // Turn off all LEDs initially
  }
}

void idleBulb() {
  delay(4);
    
  unsigned long currentMillis = millis(); // Get current millis()

    for (int i = 0; i < num_bulbs; i++) {
      float thisLedTime = currentMillis/float(3000);
      thisLedTime += float(i);
      float lightValue = sin(thisLedTime)*float(127)+float(127);

      if(i==0)Serial.println(lightValue);
      
      analogWrite(bulbs[i], lightValue);
    }
}

void loop() {
idleBulb();
} 
