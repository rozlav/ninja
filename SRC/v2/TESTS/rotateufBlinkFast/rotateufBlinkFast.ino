#include "AiEsp32RotaryEncoder.h"
#include "Arduino.h"
#include <FastLED.h>

#define ROTARY_ENCODER_A_PIN 25 // DT board MARRON Ninja
#define ROTARY_ENCODER_B_PIN 33 // CLK JAUNE board Ninja
#define ROTARY_ENCODER_BUTTON_PIN 26 // Bouton BLEU SWitch click board Ninja
#define ROTARY_ENCODER_VCC_PIN -1
#define ROTARY_ENCODER_STEPS 4

#define NUM_GAME_LEDS 12
#define GAME_LEDS_DATA_PIN 22

CRGB gameLeds[NUM_GAME_LEDS];

//instead of changing here, rather change numbers above
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN, ROTARY_ENCODER_STEPS);

int bulbs[] = {17, 16, 4, 0, 2, 15}; // bulbs pinout
int num_bulbs = sizeof(bulbs) / sizeof(bulbs[0]);

int magnets[] = {18, 19, 13, 12, 27, 14}; // magnets pinout
int num_magnets = sizeof(magnets) / sizeof(magnets[0]);


void lightAllBulbs() {
    for (int i = 0; i < num_bulbs; i++) {
      digitalWrite(bulbs[i], HIGH);
    }
}

void shutAllBulbs() {
    for (int i = 0; i < num_bulbs; i++) {
      digitalWrite(bulbs[i], LOW);
    }
}

void attachAllMagnets() {
    for (int i = 0; i < num_magnets; i++) {
      digitalWrite(magnets[i], HIGH);
    }
}

void looseAllMagnets() {
    for (int i = 0; i < num_magnets; i++) {
      digitalWrite(magnets[i], LOW);
    }
}

bool flagStateButton = false;
void rotary_onButtonClick()
{
  if (!flagStateButton) {
    lightAllBulbs();
    attachAllMagnets();
    flagStateButton = true;
  }
  else {
    looseAllMagnets();
    shutAllBulbs();
    flagStateButton = false;
  }
}

void upDateLedRing(){
  for (int i = 0; i < NUM_GAME_LEDS; i++)
  {
    if (i < rotaryEncoder.readEncoder()) {
      gameLeds[i] = CRGB::Red;
    }
    else {
      gameLeds[i] = CRGB::Black;
    }
  }
  FastLED.show();
}

void rotary_loop()
{
    if (rotaryEncoder.encoderChanged())
    {
            Serial.print("Value: ");
            Serial.println(rotaryEncoder.readEncoder());
            upDateLedRing();
    }
    if (rotaryEncoder.isEncoderButtonClicked())
    {
            rotary_onButtonClick();
    }
}

void IRAM_ATTR readEncoderISR()
{
    rotaryEncoder.readEncoder_ISR();
}


void setup()
{
    Serial.begin(115200);

    FastLED.addLeds<NEOPIXEL, GAME_LEDS_DATA_PIN>(gameLeds, NUM_GAME_LEDS);

    for (int i = 0; i < num_magnets; i++) {
      pinMode(magnets[i], OUTPUT);
    }
    for (int i = 0; i < num_bulbs; i++) {
      pinMode(bulbs[i], OUTPUT);
    }
      

    //we must initialize rotary encoder
    rotaryEncoder.begin();
    rotaryEncoder.setup(readEncoderISR);
    //set boundaries and if values should cycle or not
    //in this example we will set possible values between 0 and 1000;
    bool circleValues = false;
    rotaryEncoder.setBoundaries(0, 12, false); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)

    /*Rotary acceleration introduced 25.2.2021.
   * in case range to select is huge, for example - select a value between 0 and 1000 and we want 785
   * without accelerateion you need long time to get to that number
   * Using acceleration, faster you turn, faster will the value raise.
   * For fine tuning slow down.
   */
    rotaryEncoder.disableAcceleration(); //acceleration is now enabled by default - disable if you dont need it
    //rotaryEncoder.setAcceleration(250); //or set the value - larger number = more accelearation; 0 or 1 means disabled acceleration

    delay(500);
}

void loop()
{
    //in loop call your custom function which will process rotary encoder values
    rotary_loop();
    delay(50); //or do whatever you need to do...
}
