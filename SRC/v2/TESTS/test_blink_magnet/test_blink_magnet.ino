
#define LED1 0
#define LED2 2
#define LED3 4
#define LED4 15
#define LED5 16
#define LED6 17
#define MAGNET0 9
#define MAGNET1 10
#define MAGNET2 12
#define MAGNET3 13
#define MAGNET4 14
#define MAGNET5 27

void setup() {
  // initialize digital pin magnets & leds as an output.
  pinMode(MAGNET0, OUTPUT);
  pinMode(MAGNET1, OUTPUT);
  pinMode(MAGNET2, OUTPUT);
  pinMode(MAGNET3, OUTPUT);
  pinMode(MAGNET4, OUTPUT);
  pinMode(MAGNET5, OUTPUT);

  pinMode(LED1,OUTPUT);
  pinMode(LED2,OUTPUT);
  pinMode(LED3,OUTPUT);
  pinMode(LED4,OUTPUT);
  pinMode(LED5,OUTPUT);
  pinMode(LED6,OUTPUT);
}


void loop() {

  
  delay(500);
  digitalWrite(LED1,HIGH);
  delay(500);
  digitalWrite(LED1,LOW);
  delay(500);
  digitalWrite(LED2,HIGH);
  delay(500);
  digitalWrite(LED2,LOW);
  delay(500);
  digitalWrite(LED3,HIGH);
  delay(500);
  digitalWrite(LED3,LOW);
  delay(500);
  digitalWrite(LED4,HIGH);
  delay(500);
  digitalWrite(LED4,LOW);
  delay(500);
  digitalWrite(LED5,HIGH);
  delay(500);
  digitalWrite(LED5,LOW);
  delay(500);
  digitalWrite(LED6,HIGH);
  delay(500);
  digitalWrite(LED6,LOW);
  delay(500);

  
  delay(5000);   // wait for 10 second
  // digitalWrite(MAGNET0, HIGH);   // turn the magnet on (HIGH is the voltage level)
  // digitalWrite(MAGNET1, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET2, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET3, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET4, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET5, HIGH);   // turn the magnet on (HIGH is the voltage level)
  delay(5000);  // wait for 10 second                       
  // digitalWrite(MAGNET0, LOW);    // turn the magnet off by making the voltage LOW
  // digitalWrite(MAGNET1, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET2, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET3, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET4, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET5, LOW);    // turn the magnet off by making the voltage LOW


}
