/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

#define MAGNET10 10
#define MAGNET9 9
#define MAGNET13 13
#define MAGNET12 12
#define MAGNET14 14
#define MAGNET27 27

#define LED17 17
#define LED16 16
#define LED4 4
#define LED0 0
#define LED2 2
#define LED15 15

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin magnets & leds as an output.
  pinMode(MAGNET10, OUTPUT);
  pinMode(MAGNET9, OUTPUT);
  pinMode(MAGNET13, OUTPUT);
  pinMode(MAGNET12, OUTPUT);
  pinMode(MAGNET14, OUTPUT);
  pinMode(MAGNET27, OUTPUT);

  pinMode(LED17,OUTPUT);
  pinMode(LED16,OUTPUT);
  pinMode(LED4,OUTPUT);
  pinMode(LED0,OUTPUT);
  pinMode(LED2,OUTPUT);
  pinMode(LED15,OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED17,LOW);
  digitalWrite(LED16,LOW);
  digitalWrite(LED4,LOW);
  digitalWrite(LED0,LOW);
  digitalWrite(LED2,LOW);
  digitalWrite(LED15,LOW);
  
  delay(10000);   // wait for 10 second
  digitalWrite(MAGNET10, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET9, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET13, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET12, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET14, HIGH);   // turn the magnet on (HIGH is the voltage level)
  digitalWrite(MAGNET27, HIGH);   // turn the magnet on (HIGH is the voltage level)
  delay(10000);  // wait for 10 second                       
  digitalWrite(MAGNET10, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET9, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET13, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET12, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET14, LOW);    // turn the magnet off by making the voltage LOW
  digitalWrite(MAGNET27, LOW);    // turn the magnet off by making the voltage LOW

}
