module u() {
difference() {
  difference(){
  translate([15,-5,-110])
  cylinder(h = 200, r = 15,$fn=100);
  ;
  }
  translate([-1,0,-1])
  cube( size= [32,5,32], center=false);
}
}

module trou_vis (w,l,  diam) {
  translate([w,100,l])
  rotate([90,0,0])
    cylinder(h = 999, r = diam/2,$fn=100, center=true);
}


// u();

module vis4(w, l, socle_w_margin, diam=6) {
  ww =(w/2)+(socle_w_margin/4);
  ll = l/4;
  trou_vis( ww,  ll, diam);
  trou_vis(-ww,  ll, diam);
  trou_vis( ww, -ll, diam);
  trou_vis(-ww, -ll, diam);
}

module vis2(w, l, socle_w_margin, diam=6) {
  ww =(w/2)+(socle_w_margin/4);
  ll = l/16;
  trou_vis( ww,  0, diam);
  trou_vis(-ww,  0, diam);
}
// difference(){
//   translate([-10,15,])
//   cube( size= [50,3,30], center=false);
// }

module half_cyl(l=30,w=30) {
  difference(){
    cylinder(h = l, r = w/2,$fn=100, center=true);
    translate([0-1,-w/4-1,0])
      cube( size= [w+2,w/2+2,l+2], center=true);
  }
}
module socle_douille(w=33,l=30,h=15,bottom_margin=10, top_border=0){

  total_h = w/2 + bottom_margin;
  socle_w_margin = 30;
  socle_h = 3;


  // trou serre flex
  module trou_serre_flex(serre_flex_h=4,serre_flex_w=8){
    trou_h = socle_h + bottom_margin/3;
    translate([0, trou_h,0])
      cube( size= [999, serre_flex_h,serre_flex_w], center=true);
  }
  // U
  module U() {
    // translate([0,bottom_margin/2,0])
      // cube( size= [w+top_border,bottom_margin,l], center=true);
    difference(){
      difference() {
          half_cyl(w=40, l=l);
        translate([0,total_h,-100])
          cylinder(h = 200, r = w/2,$fn=100);
      }
    //top crop
      translate([0, 30,0])
        cube( size= [w+10+top_border,bottom_margin+10,l+10], center=true);
    }
  }
  module U_avec_trou_serre_flex(){
    difference(){
      U();
      trou_serre_flex();
    }
  }

  // socle vis
  module socle_vis(h) {
    difference() {
      translate([0,h/2,0])
        cube( size= [w+top_border+socle_w_margin, h,l], center=true);
      vis4(w,l, socle_w_margin, diam=3);
    }
  }

  module socle_vis_round(h) {
    difference() {
      // translate([0,h/2,0])
      intersection() {
        translate([0, h/2,0])
        cube( size= [w+top_border+socle_w_margin, h,l], center=true);
        translate([0, h/2,0])
        rotate([90,0,0])
      cylinder(h, r=30, center=true);
      }
      vis2(w,l, socle_w_margin, diam=3);
    }
  }
  socle_vis_round(socle_h);
  U_avec_trou_serre_flex();
}
socle_douille(l=19);
