/*
WTFPL
what the fuck you want license
DISCLAIMER :
*/

#include "Arduino.h"

// input button pinout
#define BUTTON_1_PIN 25
#define BUTTON_2_PIN 33
#define BUTTON_3_PIN 26
#define BUTTON_4_PIN 32
#define BUTTON_5_PIN 22

int bulbs[] = { 15, 2, 0, 4, 17, 16}; // bulbs pinout
int num_bulbs = sizeof(bulbs) / sizeof(bulbs[0]);

int magnets[] = {18, 19, 13, 12, 27, 14}; // magnets pinout
int num_magnets = sizeof(magnets) / sizeof(magnets[0]);

int selected_level = -1;

// idle animation : light bulb slowly ondulate
void idleBulb() {
  delay(5);
  unsigned long currentMillis = millis(); // Get current millis()
  for (int i = 0; i < num_bulbs; i++) {
    float thisLedTime = currentMillis/float(3000);
    thisLedTime += float(i);
    float lightValue = sin(thisLedTime)*float(127)+float(127);
    analogWrite(bulbs[i], lightValue);
  }
}

// LIGHT BULBS functions

void lightAllBulbs() {
    for (int i = 0; i < num_bulbs; i++) {
      analogWrite(bulbs[i], 255);
    }
}
void shutAllBulbs() {
    for (int i = 0; i < num_bulbs; i++) {
      analogWrite(bulbs[i], 0);
    }
}

// LIGHT EFFECTS

void triggerStrobeLight() {
   for(int i = 0; i < 12; i++){
      int randomLight = random(0, num_magnets);
      analogWrite(bulbs[randomLight], 255);
      delay(50);
      analogWrite(bulbs[randomLight], 0);
      delay(50);
   }
}

// MAGNETS
void attachAllMagnets() {
    for (int i = 0; i < num_magnets; i++) {
      digitalWrite(magnets[i], HIGH);
    }
}

void dropStick( int repeat,
                int numberToDrop,
                bool IsLight,
                bool fakeLight,
                bool strobeLight,
                float speedFactor,
                int lightDelay){

  for(int k = 0; k<repeat; k++){

      bool choosenStick[] = {false,false,false,false,false,false};
    
      // choose sticks indexes
      int stickToDrop[num_magnets];
    
      // randomize order of sticks
      for(int i = 0; i< numberToDrop; i++){
        stickToDrop[i] = random(0, num_magnets);
        while (digitalRead(magnets[stickToDrop[i]]) == LOW || choosenStick[stickToDrop[i]] == true) {
          stickToDrop[i] = random(0, num_magnets);
        }
        choosenStick[stickToDrop[i]] = true;
      }
      
      // delay before dropping
      int delay_min = (int)(1000 * speedFactor);
      int delay_max = (int)(4500 * speedFactor);
      delay(random(delay_min, delay_max));
    
      // switch on lights
      if( !strobeLight){
        if(IsLight){
          for(int i = 0; i<numberToDrop; i++){
            if(!fakeLight){
              analogWrite(bulbs[stickToDrop[i]], 255);
              Serial.print("light ");
              Serial.println(stickToDrop[i]);
            }else{
              int randomLight = random(0, num_magnets);
              analogWrite(bulbs[randomLight], 255);
              Serial.print("light ");
              Serial.println(randomLight);
            }
          }
        }
  
        // light delay
        delay(lightDelay);

     // strobe light   
     }else{
	     triggerStrobeLight();
     }
    
      // drop it like its hot
      for(int i = 0; i<numberToDrop; i++){
        digitalWrite(magnets[stickToDrop[i]], LOW);
        Serial.print("drop stick : ");
        Serial.println(stickToDrop[i]);
      }
    
      // shut down lights
      shutAllBulbs();
    }
}

void start_game(int selected_level) {

  shutAllBulbs();
  
  // wait
  delay(2000);

  // read the fucking manual :
  // dropStick( int repeat, // how many drop to do (1-6)
  //            int numberToDrop, // how many sticks to drop together (1-6)
  //            bool IsLight, // is there light (on/off)
  //            bool fakeLight, // is the light a fake one (on/off)
  //            bool strobe // light is strobing (on/off)
  //            float speedFactor, // delay multiplier ( 1 => between 1000 and 4500 ms delay)
  //            int lightdelay // how long light stays on, in ms
  
  // GAME DESIGN happens here :
  switch (selected_level){

    case 1:
      dropStick(6, 1, true, false, false, 0.7, 250);
      break;

    case 2:
      dropStick(6, 1, true, false, false, 0.55, 110);
      break;

    case 3:
      dropStick(4, 1, true, false, false, 0.4, 75);
      dropStick(1, 1, true, false, true, 0.4, 75);
      dropStick(1, 1, true, false, false, 0.4, 75);
      break;

    case 4:
      dropStick(1, 1, true, false, true, 0.35, 60);
      dropStick(1, 2, true, false, false, 0.35, 60);
      dropStick(3, 1, true, false, false, 0.35, 60);
      break;

    case 5:
      dropStick(1, 1, true, true, false, 0.3, 50); 
      dropStick(1, 2, true, false, true, 0.3, 50);           
      dropStick(1, 1, true, true, false, 0.15, 50);
      dropStick(1, 1, true, true, false, 0.15, 50);
      dropStick(1, 1, true, true, false, 0.15, 50);
      break;
  }

  // delay to be sure that last magnet actually turns off
  delay(1000);

  // game is finished
  Serial.println("game over");

  // set all magnet ON again
  attachAllMagnets();
}

int buttonLevelSelector() {
	int selectedLevel = -1;
  // Read the state of each button
  int button1State = digitalRead(BUTTON_1_PIN);
  int button2State = digitalRead(BUTTON_2_PIN);
  int button3State = digitalRead(BUTTON_3_PIN);
  int button4State = digitalRead(BUTTON_4_PIN);
  int button5State = digitalRead(BUTTON_5_PIN);

  // Check which button is pressed and set the selected level accordingly
  if (button1State == LOW) { 
    selectedLevel = 1;
  } else if (button2State == LOW) {
    selectedLevel = 2;
  } else if (button3State == LOW) {
    selectedLevel = 3;
  } else if (button4State == LOW) {
    selectedLevel = 4;
  } else if (button5State == LOW) {
    selectedLevel = 5;
  }

  selected_level = selectedLevel;
  delay(50);
  return  selectedLevel;
}

void setup(){
    Serial.begin(115200);

    // set-up output pins
    for (int i = 0; i < num_magnets; i++) {
      pinMode(magnets[i], OUTPUT);
    }
    for (int i = 0; i < num_bulbs; i++) {
      pinMode(bulbs[i], OUTPUT);
    }

    // make all magnet ON at start
    attachAllMagnets();

		pinMode(BUTTON_1_PIN, INPUT_PULLUP);
		pinMode(BUTTON_2_PIN, INPUT_PULLUP);
		pinMode(BUTTON_3_PIN, INPUT_PULLUP);
		pinMode(BUTTON_4_PIN, INPUT_PULLUP);
		pinMode(BUTTON_5_PIN, INPUT_PULLUP);
    
    // blink lights at start to show that we are "online"
    delay(300);
    lightAllBulbs();
    delay(500);
    shutAllBulbs();
    delay(300);
    lightAllBulbs();
    delay(500);
    shutAllBulbs();

    // ( "I know coding"). 
}

void loop(){
    idleBulb();
    buttonLevelSelector();
    if ( selected_level != -1 )
      start_game(selected_level);
    selected_level = -1;
}
