# autodoc'

## pad de notes
https://md.globenet.org/mG32P_LjSjS05-utqnaK0g?edit=

## Tofs de NinjWiggle en actions 
![action.gif](TOFS/action.gif)
![photo-1er-test-roches-zero-1.jpeg](TOFS/photo-1er-test-roches-zero-1.jpeg)
![photo-1er-test-roches-zero-2.jpeg](TOFS/photo-1er-test-roches-zero-2.jpeg)

## Améliorations
 - PCB custom ( @otto va tester sur kicad )
 - utiliser l'animation "firework" de wled avant de faire tomber le baton ?
   - code originial: https://www.anirama.com/1000leds/1d-fireworks/
   - code wled: https://github.com/Aircoookie/WLED/blob/acf6736afdc579e0bb498b9111e2b8ff7cbaab63/wled00/FX.cpp#L3350
## 2024-03-16
 - fixme
 - prototype terminé à la CNC, en état de marche et avec bande LED synchronisée !

## 2024-03-15
 - 🏁 programme du jour : souder, attacher les aimants, fignoler le code pour des 1ers tests au zéro !
 - 🏁 ajout de trois niveaux de difficulte. Le dernier il est grave dure

## 2024-03-14
 - ✅ faire fonctionner 5 aimants à la suite
 - ✅ projet arduino avec des delays simples
 - ✅ constru d'un prototype avec armature aluminium souple, à attacher avec crochet+corde
 
## 2024-03-13 Notes divers
 - ✅ 1ers tests arduino & application du schéma électronique 
 - ❌ les « [drivers](https://fr.aliexpress.com/item/1005006005913440.html?gatewayAdapt=glo2fra) » c'est que pour les leds, overkill pour les aimants
